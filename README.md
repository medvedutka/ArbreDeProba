Application "Web" (HTML/Javascript) vous permettant de définir et d'afficher un arbre de probabilité.

# Fonctionnalités:
- Définition de l'arbre par une syntaxe simple
- Zoom/panning
- Choix de la couleur des arètes
- Configuration de l'écartement vertical et horizontal des noeuds
- Affichage d'un titre pour chaque étapes
- Possibilité d'enregistrer l'arbre au format PNG


# Syntaxe de définition de l'arbre:
La syntaxe est reprise de l'application "Geophar" ( http://geophar.sourceforge.net ). Un exemple est fournis au départ.

    ||titre1|titre2|titre3
    Noeud racine
    >sous-noeud:proba1
    >>sous-noeud:proba2
    >>>sous-noeud:proba3
    >>>sous-noeud:proba4
    >>sous-noeud:proba5
    


# Aspect technique:
- Application cliente autonome (js/html)
- Utilisation des bibliothèques tierce:
    - D3 ( https://d3js.org )
    - jscolor ( http://jscolor.com )
    - pablo ( http://pablojs.com )
    - simg ( http://krunkosaurus.github.io/simg )
    - Split.js ( http://nathancahill.github.io/Split.js )

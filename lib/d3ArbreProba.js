function D3ArbreProba() {
    this.height = "100%";
    this.i = 0;
    this.radius = 20;
    this.svgHolder = null;
    this.treeRoot = null;
    this.tree = null;
    this.topG = null;
    this.svg = null;
    this.depthLabels = [];
    this.depthLabelsY = 0;
    this.linkColor = "blue";
    this.backgroundColor = null;
    this.horizontalNodeSeparation = 100;
    this.verticalNodeSeparation = 0.7;
};


D3ArbreProba.prototype.init = function (_svgHolder, _treeRoot, _depthLabels, _linkColor, _backgroundColor, _horizontalNodeSeparation, _verticalNodeSeparation) {
    this.svgHolder = _svgHolder;
    this.treeRoot = _treeRoot;
    if (_depthLabels !== undefined && _depthLabels !== null) {
        this.depthLabels = _depthLabels;
    }
    if (_linkColor !== undefined && _linkColor !== null) {
        this.linkColor = _linkColor;
    }
    if (_backgroundColor !== undefined && _backgroundColor !== null && _backgroundColor !== "") {
        this.backgroundColor = _backgroundColor;
    }
    if (_verticalNodeSeparation !== undefined && _verticalNodeSeparation !== null) {
        this.verticalNodeSeparation = _verticalNodeSeparation;
    }
    if (_horizontalNodeSeparation !== undefined && _horizontalNodeSeparation !== null) {
        this.horizontalNodeSeparation = _horizontalNodeSeparation;
    }

    var _this = this;

    this.tree = d3.layout.tree().nodeSize([40, -1]).separation(function (a, b) {
        return _this.verticalNodeSeparation;
    });

    this.svg = this.svgHolder.append("svg")
            .attr("height", this.height)
            .attr("width", "100%")
            .call(d3.behavior.zoom().on("zoom", function () {
                _this.topG.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
            }));

    this.topG = this.svg.append("g")
            .attr("id", "topg")
            .attr("class", "topg");

    /*if(this.backgroundColor !== null) {
     this.topG.append("rect")
     .attr("width","200%")
     .attr("height","200%")
     .attr("x","-100%")
     .attr("fill",this.backgroundColor);
     }*/


    this.update();
};

D3ArbreProba.prototype.update = function () {
    // Pass in the data structure "data" d3 creates a visual tree layout using that data
    // Links are the source and target locations for the lines between the circles
    var nodes = this.tree.nodes(this.treeRoot).reverse();
    var links = this.tree.links(nodes);

    // This tree will be evenly spaced fixed depth with each level 250px from the previous
    // To compress or enlarge the tree, change this number
    var _this = this;
    var depthXs = [];
    nodes.forEach(function (d) {
        d.y = d.depth * _this.horizontalNodeSeparation;
        depthXs[d.depth] = d.y;
        if (_this.depthLabelsY > d.x) {
            _this.depthLabelsY = d.x;
        }
    });

    for (var d = 0; d < depthXs.length - 1; d++) {
        var depthLabel = _this.depthLabels[d];
        if (depthLabel !== undefined && depthLabel !== null && typeof depthLabel === "string" && typeof depthXs[d] === "number" && typeof depthXs[d + 1] === "number") {
            var dX = depthXs[d] + Math.round((Math.abs(depthXs[d] - depthXs[d + 1]) / 2));
            _this.depthLabels[d] = {x: dX, label: depthLabel};
        }
    }


    // Create the invdidual Nodes on the tree and bind/join them to you data structure
    // Each element in your data structure "data" is assigned to a Node
    var node = this.topG.selectAll("g.node").data(nodes, function (d) {
        if (d.id) {
            return d.id;
        } else {
            d.id = this.i++;
            return d.id;
        }
    });

    var radius = this.radius;
    var topG = this.topG;
    var linkPathId = 0;

    nodes.forEach(function (node) {
        var nodeText = topG.append("text")
                .attr("class", "nodeText notNode")
                .attr("x", node.y)
                .attr("y", node.x)
                .attr("dy", ".4em")
                .attr("text-anchor", "middle");
        if (node.label.length > 0 && node.label[0] === "&") {
            nodeText.attr("text-decoration", "overline");
            nodeText.text(node.label.substring(1));
        } else {
            nodeText.text(node.label);
        }
    });

    var linkColor = this.linkColor;
    links.forEach(function (link) {
        var lineFunction = d3.svg.line()
                .x(function (d) {
                    return d.y;
                })
                .y(function (d) {
                    return d.x;
                })
                .interpolate("linear");

        var linkId = "linkPathId" + (++linkPathId);
        var lineGraph = topG.append("path")
                .attr("id", linkId)
                .attr("d", lineFunction([{x: link.source.x, y: link.source.y + radius}, {x: link.target.x, y: link.target.y - radius}]))
                .attr("stroke", linkColor)
                .attr("stroke-width", 2)
                .attr("fill", "none");

        var dy = "-3";
        if (link.source.x < link.target.x) {
            dy = "1em";
        }
        topG.append("text")
                .attr("dy", dy)
                .attr("text-anchor", "middle")
                .append("textPath")
                .attr("xlink:href", "#" + linkId)
                .attr("startOffset", "50%")
                .text(link.target.parentToThisLinkLabel);

        /*var nodeEnd = topG.append("circle")
         .attr("id","node_" +linkId)
         .attr("cx",link.target.y)
         .attr("cy",link.target.x)
         //.attr("class", "node")
         .attr("r", radius);*/
        /*var nodeText = topG.append("text")
         .attr("class", "nodeText")
         .attr("x",link.target.y)
         .attr("y",link.target.x)
         .attr("dy",".4em")
         .attr("text-anchor","middle")
         .text(link.source.label);*/
        //var textWidth = nodeText.node().getBBox().width;
        //nodeEnd.attr("r",textWidth);
    });

    for (var i = 0; i < this.depthLabels.length; i++) {
        var nodeText = topG.append("text")
                .attr("class", "nodeText")
                .attr("x", this.depthLabels[i].x)
                .attr("y", this.depthLabelsY)
                .attr("dy", "-2em")
                .attr("text-anchor", "middle")
                .text(this.depthLabels[i].label);
    }

    var bbox = this.topG.node().getBBox();
    this.svg.attr("transform", "translate(" + 50 + "," + ((bbox.height / 2) + 30) + ")");
};


function saveSVGAsPNG(svgNode) {
    var html = svgNode
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .node().parentNode.innerHTML;

    var imgsrc = 'data:image/svg+xml;base64,' + btoa(html);
    var img = '<img src="' + imgsrc + '">';
    d3.select("#svgdataurl").html(img);

    var canvas = document.createElement("canvas");
    var context = canvas.getContext("2d");

    var image = new Image();
    image.src = imgsrc;
    image.onload = function () {
        context.drawImage(image, 0, 0);

        var canvasdata = canvas.toDataURL("image/png");

        var pngimg = '<img src="' + canvasdata + '">';
        d3.select("#pngdataurl").html(pngimg);

        var a = document.createElement("a");
        a.download = "sample.png";
        a.href = canvasdata;
        a.click();
    };
}

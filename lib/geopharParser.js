function GeopharScript() {
    this.levelLabels = [];
    this.root = null;
    this._graph = {nodes: [], edges: []}
    this.idNode = 0;
}

GeopharScript.prototype.parseGeopharFormat = function (geoGraph) {
    geoGraph = geoGraph.replace("\r\n", "\n");
    var lines = geoGraph.split("\n");
    // First line may contain level description
    var indexBegining = 0;
    var firstLine = lines[0];
    if (firstLine.substring(0, 2) === "||") {
        indexBegining = 1;
        firstLine = firstLine.substring(2);
        var levelDesc = firstLine.split("|");
        for (var i = 0; i < levelDesc.length; i++) { //sanitize
            if (levelDesc[i] === null || levelDesc[i] === undefined) {
                levelDesc[i] = "";
            }
        }
        this.levelLabels = levelDesc;
    }

    var stack = [];
    // Second line contains root name
    var root = lines[indexBegining].trim();

    var current = {level: 0, node: this.createNode(root)};
    stack.push(current);
    for (var i = indexBegining + 1; i < lines.length; i++) {
        var line = lines[i].trim();
        if (line === "") {
            continue;
        }
        var geophar = this.parseGeopharLine(line);
        var node = this.createNode(geophar.value);
        if (this.root === null) {
            this.root = node;
        }
        if (geophar.level > current.level) {
            this.createEdge(current.node, node, geophar.edge);
            current = {level: geophar.level, node: node};
            stack.push(current);
        } else if (geophar.level < current.level) {
            //pop until good level then push
            while (current.level !== geophar.level - 1) {
                stack.pop();
                current = stack[stack.length - 1];
            }
            this.createEdge(current.node, node, geophar.edge);
            current = {level: geophar.level, node: node};
            stack.push(current);
        } else { // levels are equals, must pop to find parent
            //pop then and push
            while (current.level !== (geophar.level - 1)) {
                stack.pop();
                current = stack[stack.length - 1];
            }
            this.createEdge(current.node, node, geophar.edge);
            current = {level: geophar.level, node: node};
            stack.push(current);
        }
    }
};

GeopharScript.prototype.toD3Tree = function () {
    var roots = this.findRoots();
    var root = roots[0];
    var result = this.toD3Tree_internal(root);
    return result;
};

GeopharScript.prototype.toD3Tree_internal = function (node) {
    var d3Node = {label: node.label, parent: null, children: []};
    var edges = this.findAllEdgeStartingFrom(node);
    for (var i = 0; i < edges.length; i++) {
        var edge = edges[i];
        var d3TargetNode = this.toD3Tree_internal(edge.nodeEnd);
        d3TargetNode.parentToThisLinkLabel = edge.label;
        d3TargetNode.parent = d3Node;
        d3Node.children.push(d3TargetNode);
    }
    return d3Node;
};

GeopharScript.prototype.toJitJson = function () {
    var result = [];
    for (var i = 0; i < this._graph.nodes.length; i++) {
        var node = this._graph.nodes[i];
        result.push(this.toJitJson_internal(this._graph, node));
    }
    return result;
};

GeopharScript.prototype.toJitJson_internal = function (node) {
    var result = {id: node.id, name: node.label, adjacencies: []};
    var edges = this.findAllEdgeStartingFrom(node, this._graph);
    for (var i = 0; i < edges.length; i++) {
        var edge = edges[i];
        result.adjacencies.push({nodeTo: edge.nodeEnd.id, data: {label: edge.label}});
    }
    return result;
};

GeopharScript.prototype.toJitJsonSimple = function () {
    var roots = this.findRoots();
    return this.toJitJson_internal(roots[0]);
};

GeopharScript.prototype.toJitJsonSimple_internal = function (node) {
    var result = {id: node.id, name: node.label, children: []};
    var childs = this.findAllChilds(node);
    for (var i = 0; i < childs.length; i++) {
        result.children.push(this.toJitJson_internal(childs[i]));
    }
    return result;
};

GeopharScript.prototype.findAllChilds = function (node) {
    var childs = [];
    for (var i = 0; i < this._graph.edges.length; i++) {
        var e = this._graph.edges[i];
        if (node === e.nodeStart) {
            childs.push(e.nodeEnd);
        }
    }
    return childs;
};

GeopharScript.prototype.findAllEdgeStartingFrom = function (node) {
    var edges = [];
    for (var i = 0; i < this._graph.edges.length; i++) {
        var e = this._graph.edges[i];
        if (node === e.nodeStart) {
            edges.push(e);
        }
    }
    return edges;
};

GeopharScript.prototype.findAllAncestors = function (node) {
    var childs = [];
    for (var i = 0; i < this._graph.edges.length; i++) {
        var e = this._graph.edges[i];
        if (node === e.nodeEnd) {
            childs.push(e.nodeStart);
        }
    }
    return childs;
};

GeopharScript.prototype.findRoots = function () {
    var roots = [];
    for (var i = 0; i < this._graph.nodes.length; i++) {
        roots.push(this._graph.nodes[i]);
    }
    for (var i = 0; i < this._graph.edges.length; i++) {
        var e = this._graph.edges[i];
        var index = roots.indexOf(e.nodeEnd);
        if (index !== -1) {
            roots.splice(index, 1);
        }
    }
    return roots;
};

GeopharScript.prototype.toDot = function () {
    var r = "digraph graphname {\n";
    for (var i = 0; i < this._graph.nodes.length; i++) {
        var n = this._graph.nodes[i];
        r += n.id + " [label=\"" + n.label + "\"];\n";
    }
    for (var i = 0; i < this._graph.edges.length; i++) {
        var e = this._graph.edges[i];
        r += e.nodeStart.id + " -> " + e.nodeEnd.id + " [label=\"" + e.label + "\"];\n";
    }
    r += "\n}";
    return r;
};

GeopharScript.prototype.createNode = function (label) {
    var n = {id: "Node" + this.idNode++, label: label};
    this._graph.nodes.push(n);
    return n;
};

GeopharScript.prototype.createEdge = function (n1, n2, label) {
    var e = {nodeStart: n1, nodeEnd: n2, label: label};
    this._graph.edges.push(e);
    return e;
};

GeopharScript.prototype.parseGeopharLine = function (line) {
    var result = {level: 0, edge: "", value: ""};
    var n = count(">", line);
    result.level = n;
    var labels = line.substring(n).split(":");
    result.edge = labels[1].trim();
    result.value = labels[0].trim();
    return result;
};

function count(c, str) {
    var i = 0;
    while (str[i] === c) {
        i = i + 1;
    }
    return i;
};

function logObject(obj) {
    var seen = [];
    var strObj = JSON.stringify(obj, function (key, val) {
        if (val !== null && typeof val === "object") {
            if (seen.indexOf(val) >= 0) {
                return "loopObjectDetected";
            }
            seen.push(val);
        }
        return val;
    });
    console.log(strObj);
}
